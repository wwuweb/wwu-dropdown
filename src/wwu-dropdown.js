/**
 * @file Source code for WWU Dropdown module.
 * @author Nigel Packer
 *
 * @namespace wwu
 */
var wwu = (function ($, wwu, window, document, undefined) {

  'use strict';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var PLUGIN = 'wwu-dropdown';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var ENABLED = PLUGIN + '-enabled';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var DISABLED = PLUGIN + '-disabled';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var CLOSING = PLUGIN + '-closing';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var OPENED = PLUGIN + '-opened';

  /**
   * @constant
   * @type {string}
   * @default
   */
  var OPENING = PLUGIN + '-opening';

  /**
   * Click event handler for list items containing submenus.
   * @function click_submenu_parent
   * @param {Event} event
   * @private
   */
  function click_submenu_parent(event) {
    var $target = $(event.currentTarget);

    interrupt_animation($target);

    if ($target.hasClass(OPENED)) {
      close_submenu($target);
    }
    else {
      close_submenu($target.siblings('.' + OPENED));
      open_submenu($target);
    }

    event.stopPropagation();
  }

  /**
   * Click event handler for links in list items containing submenus.
   *
   * Prevents the default link action from occurring.
   * @function click_submenu_parent_link
   * @param {Event} event
   * @private
   */
  function click_submenu_parent_link(event) {
    event.preventDefault();
  }

  /**
   * Mouseenter event handler for list items containing submenus.
   * @function mouseenter_submenu_parent
   * @param {Event} event
   * @private
   */
  function mouseenter_submenu_parent(event) {
    var $target = $(event.currentTarget);

    interrupt_animation($target);

    if (!$target.hasClass(OPENED)) {
      close_submenu($target.siblings('.' + OPENED));
      open_submenu($target);
    }
  }

  /**
   * Mouseleave event handler for list items containing submenus.
   * @function mouseleave_submneu_parent
   * @param {Event} event
   * @private
   */
  function mouseleave_submenu_parent(event) {
    var $target = $(event.currentTarget);

    interrupt_animation($target);

    if ($target.hasClass(OPENED)) {
      close_submenu($target);
    }
  }

  /**
   * Interupt an in-progress animation and force it to complete.
   * @function interrupt_animation
   * @param {jQuery} $parent - The list item containing the menu currently
   *   animating.
   * @private
   */
  function interrupt_animation($parent) {
    $parent.children('ul').stop(true, true);
  }

  /**
   * Open the menu inside of the list item that recieved an event.
   * @function open_submenu
   * @param {jQuery} $parent - The list item that received and event and
   *   containing the menu to be opened.
   * @private
   */
  function open_submenu($parent) {
    var $submenu = $parent.children('ul');

    if ($parent.hasClass(OPENING)) {
      return false;
    }

    $parent.addClass(OPENING);
    $submenu.css('zIndex', 9999);
    $submenu.fadeIn('fast', function () {
      $parent.removeClass(OPENING);
      $parent.addClass(OPENED);
    });
  }

  /**
   * Close the menu inside of the list item that received an event.
   * @function close_submenu
   * @param {jQuery} $parent - The list item that received an event and
   *   containing the menu to be opened.
   * @private
   */
  function close_submenu($parent) {
    var $submenu = $parent.find('ul');

    if ($parent.hasClass(CLOSING)) {
      return false;
    }

    $parent.removeClass(OPENED);
    $parent.addClass(CLOSING);
    $submenu.css('zIndex', 9998);
    $submenu.fadeOut('slow', function () {
      $parent.removeClass(CLOSING);
    });
  }

  /**
   * A jQuery dropdown menu module. Displays a nested unordered list of links as
   * an animated, single-level dropdown menu.
   *
   * Methods are providing for controlling the dropdown instance. These include
   * an option to disable the dropdown functionality, which preserves the
   * instance but removes all event handlers. This can be useful to responding
   * to media queries. There is also an complimentary option to (re-)enable the
   * dropdown. Finally, a method is provided to completely remove the instance,
   * which will unregister event handlers and destroy the instance.
   *
   * @example <caption>Create a new WWU Dropdown instance:</caption>
   * $('#menu').wwuDropdown();
   * @example <caption>Disable a WWU Dropdown instance:</caption>
   * $('#menu').wwuDropdown('disable');
   * @example <caption>Enable a WWU Dropdown instance:</caption>
   * $('#menu').wwuDropdown('enable');
   * @example <caption>Destroy a WWU Dropdown instance:</caption>
   * $('#menu').wwuDropdown('destroy');
   *
   * @requires jQuery
   *
   * Creates a new WWU Dropdown instance.
   * @class
   * @memberof wwu
   * @property {Element} element - The original menu element.
   * @property {jQuery} $element - The jQuery object containing the menu
   *   element.
   * @property {jQuery} $menu_submenu_parents - All list items in the menu that
   *   contain a submenu.
   * @property {jQuery} $menu_submenu_parent_links - All links directly in
   *   submenu parent list items.
   * @param {Element} element - The list to enable as a dropdown.
   */
  wwu.Dropdown = function (element) {
    this.element = element;
    this.$element = $(element);
    this.$menu_submenu_parents = this.$element.find('li').has('ul');
    this.$menu_submenu_parent_links = this.$menu_submenu_parents.children('a');

    this.$element.data(PLUGIN, this);
    this.$element.addClass(PLUGIN);
    this.$element.addClass(DISABLED);
    this.enable();
  };

  /**
   * Enable the WWU Dropdown instance.
   * @method enable
   * @public
   */
  wwu.Dropdown.prototype.enable = function () {
    if (!this.$element.hasClass(DISABLED)) {
      return;
    }

    this.$element.removeClass(DISABLED);
    this.$element.addClass(ENABLED);
    this.$menu_submenu_parents.click(click_submenu_parent);
    this.$menu_submenu_parents.mouseenter(mouseenter_submenu_parent);
    this.$menu_submenu_parents.mouseleave(mouseleave_submenu_parent);
    this.$menu_submenu_parent_links.click(click_submenu_parent_link);
  };

  /**
   * Disable the WWU Dropdown instance.
   * @method disable
   * @public
   */
  wwu.Dropdown.prototype.disable = function () {
    if (!this.$element.hasClass(ENABLED)) {
      return;
    }

    this.$element.removeClass(ENABLED);
    this.$element.addClass(DISABLED);
    this.$menu_submenu_parents.off('click', click_submenu_parent);
    this.$menu_submenu_parents.off('mouseenter', mouseenter_submenu_parent);
    this.$menu_submenu_parents.off('mouseleave', mouseleave_submenu_parent);
    this.$menu_submenu_parent_links.off('click', click_submenu_parent_link);
  };

  /**
   * Destroy the WWU Dropdown instance.
   * @method destroy
   * @public
   */
  wwu.Dropdown.prototype.destroy = function () {
    this.disable();
    this.$element.removeClass(PLUGIN);
    this.$element.removeData(PLUGIN);

    this.$menu_submenu_parent_links = null;
    this.$menu_submenu_parents = null;
    this.$element = null;
    this.element = null;
  };

  $.fn.wwuDropdown = function (options) {
    return this.each(function (index, element) {
      var instance = $.data(element, PLUGIN);

      if (instance) {
        instance[options]();
      }
      else {
        new wwu.Dropdown(element);
      }
    });
  };

  return wwu;

})(jQuery, wwu || {}, this, this.document);
